import React from 'react';
import {Link} from "react-router-dom";

const DefaultLayout = ({ children }) => {
    return (
        <>
            <header className="jo-header">
                <div>
                <p><Link to="/" ><img className="jo-logo" src={process.env.PUBLIC_URL + "bumbum.png"}/></Link></p>
                </div>
                <div>
                <nev>
                    <p><Link to="/p2" className="jo-link">두번째</Link></p>
                </nev>
                </div>
            </header>
            <main>
            {children}
            </main>
            <footer className="jo-header">
                <img className="jo-logo" src={process.env.PUBLIC_URL + "bumbum.png"}/>
            </footer>
        </>
    )
}

export default DefaultLayout;