import './App.css';
import {Route, Routes} from "react-router-dom";
import DefaultLayout from "./layouts/DefaultLayout";
import Menu from "./pages/menu";
import PageTwo from "./pages/pageTwo";
import TestPage1 from "./pages/TestPage1";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<DefaultLayout><TestPage1/></DefaultLayout>}></Route>
        <Route path="/p2" element={<DefaultLayout><PageTwo/></DefaultLayout>}></Route>
      </Routes>
    </div>
  );
}

export default App;
