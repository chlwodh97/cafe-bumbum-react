import React, {useCallback, useState} from 'react';
import CafeItem from "../components/CafeItem";
import CartMenu from "../components/CartMenu";

const TestPage1 = () => {
    const [cart , setCart] = useState([]);


    const menus = [
        {img : "/ame.jpg" ,name : "아이스 아메리카노" , price : 2000},
        {img : "/cafeLa.jpg" ,name : "카페라떼" , price : 3000},
        {img : "/nokLa.jpg" ,name : "녹차라떼" , price : 4000},
        {img : "/cara.jpg" ,name : "카라멜 마끼아또" , price : 5000},
    ]

    // 콜백함수를 자식컴포넌트한테 넘겨서 사용할 거다 -> 이런 상황에 메모이제이션 사용
    const addToCart = useCallback(menu => {
        // useState setCart 할건데 prevCart 는 받는 이름 전 cart
        setCart((prevCart) => {
            // existingItem 은 cart임(배열) find // true false 리턴
            // -> find 는 장바구니에 같은 게 있나 없나 판단
            const existingItem = prevCart.find(item => item.name === menu.name);
            if (existingItem) {
                // map 으로 순회하면서 이름이 같은애를 찾아서 있으면 + 1 해주기
                // 이게 없으면 걍 다 +1 되버림
                return prevCart.map((item) => item.name === menu.name ? {...item, quantity : item.quantity + 1} : item
                )
            } else {
                // 장바구니에 없으면 수량 1 으로 만들기
                return [...prevCart, {...menu,quantity : 1}]
            }
        })
    }, [])


    const removeFromCart = useCallback((menu) => {
        setCart((prevCart) => {
            return prevCart.reduce((acc, item) => {
                // 만약에 받는애랑 장바구니안에 있는 전 값이랑 같고
                // 같으면 밑 코드 실행
                if (item.name === menu.name) {
                    // 있는데 1보다 클 때
                    if (item.quantity > 1) {
                        // 지금까지 있던 배열 + 들어오면 객체 안에 quantity - 1
                        return [...acc,{...item, quantity: item.quantity - 1}];
                    }
                    // else 들어오는 이름이랑 삭제하려는 애랑 다를 때
                    // if (item.name === menu.name) 이녀석의 else
                } else {
                    // 들어오는 애 삭제 눌럿는데 다른애 지워지면 안되니까 같은애가 아니면 지우지마
                    return [...acc, item]
                }
                // 지금 있는 값 반환 -> 냅둬라
                return acc;
            }, [])
        })
    },[])

    const calculateTotal = useCallback(() => {
        return cart.reduce((total, item) => total + item.price * item.quantity, 0)
    },[cart])

    return(
        <div>
            <h2>카페메뉴</h2>
            <div className="com-flex">
                {menus.map(menu => (
                    <CafeItem key={menu.name} menu={menu} addToCart={addToCart}/>
                ))}
            </div>
            <h2>장바구니</h2>
            <div className="camenu-big">
                {cart.map(item => (
                    <CartMenu key={item.name} item={item} removeFromCart={removeFromCart}/>
                ))}

            </div>
            <div className="jo-all-price">
                총 금액 : {calculateTotal()} 원
            </div>
        </div>
    )
}

export default TestPage1;