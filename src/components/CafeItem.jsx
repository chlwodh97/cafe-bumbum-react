import React from 'react';

const CafeItem = ({ menu , addToCart }) => {
    return (
        <div onClick={() => addToCart(menu)} className="jo-com-boss">
            <div><img className="jo-img" src={process.env.PUBLIC_URL + menu.img}/></div>
            <div className="jo-menuName">{menu.name}</div>
            <div>{menu.price}</div>
        </div>
    )
}

export default CafeItem;