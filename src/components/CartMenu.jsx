import React from "react";

const CartMenu = ( {item , removeFromCart} ) => {
    return (
        <div className="cart-menu">
            <div className="jo-cart-menu-name">{item.name}</div>
            <p className="jo-p">수량 : {item.quantity}</p>
            <p className="jo-p">가격 : {item.quantity * item.price}</p>
            <button onClick={() => removeFromCart(item)}>삭제</button>
        </div>
    )
}

export default CartMenu;