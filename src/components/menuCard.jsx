import React from 'react';

const MenuCard = (drinkMenu) => (
    <div className="jo-menu">
        <div>
            <img className="jo-img" src={process.env.PUBLIC_URL + "ame.jpg"}/>
        </div>
        <div className="jo-menuName">
            <h2>{drinkMenu.name}</h2>
            <p>{drinkMenu.price}원</p>
        </div>
    </div>
)
export default MenuCard